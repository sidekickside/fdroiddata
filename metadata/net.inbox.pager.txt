Categories:Internet
License:GPLv3
Web Site:https://github.com/itprojects/InboxPager/blob/HEAD/README.md
Source Code:https://github.com/itprojects/InboxPager
Issue Tracker:https://github.com/itprojects/InboxPager/issues

Auto Name:Inbox Pager
Summary:Read and write e-mails
Description:
E-mail client that supports IMAP, POP and SMTP protocols using SSL/TLS.
.

Repo Type:git
Repo:https://github.com/itprojects/InboxPager.git

Build:1.0,1
    commit=28193bcd1165a9cd012dfc39cc38162c81eb4925
    subdir=InboxPager
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1
