AntiFeatures:NonFreeAdd
Categories:System
License:Apache2
Web Site:
Source Code:https://github.com/d4rken/reddit-android-appstore
Issue Tracker:https://github.com/d4rken/reddit-android-appstore/issues

Auto Name:/r/Android App store
Summary:/r/Andr0id app store
Description:
App inspired by this
[https://www.reddit.com/r/Android/comments/50rafp/meta_we_have_an_app_wiki_with_over_700_apps_made/
reddit post]

Pulls the curated app list from the [https://www.reddit.com/r/android/wiki/apps
/r/Android wiki] and displays it.

As it offers non free apps as well, this app has the non free addon antifeature.
.

Repo Type:git
Repo:https://github.com/d4rken/reddit-android-appstore.git

Build:0.3.1,3100
    commit=v0.3.1
    subdir=app
    gradle=yes

Build:0.4.0,4000
    commit=v0.4.0
    subdir=app
    gradle=yes

Build:0.5.0,5000
    commit=v0.5.0
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
